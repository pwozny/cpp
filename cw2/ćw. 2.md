
# ćw. 1

## 1

Nie kompiluje się

```cpp
pwozny@khorinis:~/cernbox/workspace/agh/cpp/ćw. 2$ g++ z1.cpp
z1.cpp: In function ‘int main()’:
z1.cpp:75:25: error: ‘std::string student::imie_nazwisko_’ is private within this context
   75 | cout << "Dane:" << stud.imie_nazwisko_ << " " << stud.nr_indeksu_ << endl; starosta star("Aleksandra Nowak",999,"mail@nomail.dot"); star.printOpis();
      |                         ^~~~~~~~~~~~~~
z1.cpp:7:8: note: declared private here
    7 | string imie_nazwisko_ = "NO_NAME";
      |        ^~~~~~~~~~~~~~
z1.cpp:75:55: error: ‘unsigned int student::nr_indeksu_’ is private within this context
   75 | cout << "Dane:" << stud.imie_nazwisko_ << " " << stud.nr_indeksu_ << endl; starosta star("Aleksandra Nowak",999,"mail@nomail.dot"); star.printOpis();
      |                                                       ^~~~~~~~~~~
z1.cpp:8:14: note: declared private here
    8 | unsigned int nr_indeksu_ = 0;
      |              ^~~~~~~~~~~
z1.cpp:77:25: error: ‘std::string student::imie_nazwisko_’ is private within this context
   77 | cout << "Dane:" << star.imie_nazwisko_ << " " << star.nr_indeksu_ << endl;
      |                         ^~~~~~~~~~~~~~
z1.cpp:7:8: note: declared private here
    7 | string imie_nazwisko_ = "NO_NAME";
      |        ^~~~~~~~~~~~~~
z1.cpp:77:55: error: ‘unsigned int student::nr_indeksu_’ is private within this context
   77 | cout << "Dane:" << star.imie_nazwisko_ << " " << star.nr_indeksu_ << endl;
      |                                                       ^~~~~~~~~~~
z1.cpp:8:14: note: declared private here
    8 | unsigned int nr_indeksu_ = 0;
      |              ^~~~~~~~~~~
```

Po zaimplementowaniu klasy oraz usunięciu błędów:

```diff
pwozny@khorinis:~/cernbox/workspace/agh/cpp/ćw. 2$ git diff
diff --git "a/\304\207w. 1/z3.cpp" "b/\304\207w. 1/z3.cpp"
index 0c22312..394b1ec 100644
--- "a/\304\207w. 1/z3.cpp"
+++ "b/\304\207w. 1/z3.cpp"
@@ -31,9 +31,8 @@ string Opis_ = "starosta grupy";
 void printDane()
 {
 cout << " Metoda printDane klasy dziedziczonej" << endl;
-cout << " imie nazwisko " << student::imie_nazwisko_ << endl;
-cout << " nr indeksu " << student::nr_indeksu_ << endl;
 cout << " adres email " << email_ << endl;
+student::printDane();
 }
 };

diff --git "a/\304\207w. 2/z1.cpp" "b/\304\207w. 2/z1.cpp"
index 4555ce2..cd34937 100644
--- "a/\304\207w. 2/z1.cpp"
+++ "b/\304\207w. 2/z1.cpp"
@@ -9,8 +9,9 @@ unsigned int nr_indeksu_ = 0;

 public:

-student(string imie_nazwisko, unsigned int nr_indeksu);
-string Opis_ = "student grupy"; void printOpis();
+student(string imie_nazwisko, unsigned int nr_indeksu);
+ string Opis_ = "student grupy";
+ void printOpis();

 void printDane()
 {
@@ -20,7 +21,18 @@ cout << " nr indeksu " << nr_indeksu_        << endl;
 }
 };

-class starosta : public student
+class funkcyjny{
+public:
+
+void poinformuj_grupe(void){
+    cout << "grupa została poinformowana\n";
+}
+void poinformuj_prowoadzacego(void){
+    cout << "Prowadzacy zostal poinformowany\n";
+}
+};
+
+class starosta : public student, public funkcyjny

 {
 private:
@@ -60,7 +72,9 @@ int main()
 student stud("Jan Kowalski",7);
 stud.printOpis();

-cout << "Dane:" << stud.imie_nazwisko_ << " " << stud.nr_indeksu_ << endl;
-starosta star("Aleksandra Nowak",999,"mail@nomail.dot"); star.printOpis();
-cout << "Dane:" << star.imie_nazwisko_ << " " << star.nr_indeksu_ << endl;
+stud.printDane();
+starosta star("Aleksandra Nowak",999,"mail@nomail.dot");
+star.printOpis();
+star.printDane();
+star.poinformuj_grupe();
```

Dodanie atrybutu _virtual_ nic w tym kontekście nie zmienia.

```diff
diff --git "a/\304\207w. 2/z2.cpp" "b/\304\207w. 2/z2.cpp"
index cd34937..e655816 100644
--- "a/\304\207w. 2/z2.cpp"
+++ "b/\304\207w. 2/z2.cpp"
@@ -13,7 +13,7 @@ student(string imie_nazwisko, unsigned int nr_indeksu);
  string Opis_ = "student grupy";
  void printOpis();

-void printDane()
+virtual void printDane()
 {
 cout << " Metoda printDane klasy bazowej" << endl;
 cout << " imie nazwisko " << imie_nazwisko_ << endl;
 ```

## 3

Program się nie kompiluje

```cpp
pwozny@khorinis:~/cernbox/workspace/agh/cpp/ćw. 2$ g++ z3.cpp
z3.cpp: In function ‘int main()’:
z3.cpp:65:12: error: cannot declare variable ‘u’ to be of abstract type ‘urzadzenie’
   65 | urzadzenie u;
      |            ^
z3.cpp:4:7: note:   because the following virtual functions are pure within ‘urzadzenie’:
    4 | class urzadzenie
      |       ^~~~~~~~~~
z3.cpp:9:13: note:      ‘virtual int urzadzenie::zapis(int, std::string)’
    9 | virtual int zapis(int id, string dane) = 0;
      |             ^~~~~
z3.cpp:10:16: note:     ‘virtual std::string urzadzenie::odczyt(int)’
   10 | virtual string odczyt(int id) = 0;
      |                ^~~~~~
```

zakomentowano tworzenie virtualnej klasy:

```diff
diff --git "a/\304\207w. 2/z3.cpp" "b/\304\207w. 2/z3.cpp"
index e5d547b..799eba8 100644
--- "a/\304\207w. 2/z3.cpp"
+++ "b/\304\207w. 2/z3.cpp"
@@ -55,7 +55,7 @@ string dysk::odczyt(int id)
 
 int main() {
 
-  urzadzenie u;
+//   urzadzenie u;
 
   dysk d1(7);
```

z rezultatem:

```bash
pwozny@khorinis:~/cernbox/workspace/agh/cpp/ćw. 2$ ./a.out
Tworzenie obiektu klasy dysk
zapis danych: test 11
odczyt danych: test 11
```

Dodanie warunku sprawdzającego wartość id:

```diff
diff --git "a/\304\207w. 2/z3.cpp" "b/\304\207w. 2/z3.cpp"
index 799eba8..920444b 100644
--- "a/\304\207w. 2/z3.cpp"     
+++ "b/\304\207w. 2/z3.cpp"     
@@ -41,8 +41,11 @@ int dysk::zapis(int id, string dane)
   dane_ = dane;
 
   cout << "zapis danych: " << dane << endl;
-
-  return 0;
+  if (id_==id) {
+    return 1;
+  }else {
+    return -1;
+  }
 }
```

