#include<iostream>
#include<vector>
using namespace std;

int main(){
vector<int> v(10);

// 2. Zmodyfikować kod programu z punktu 1 aby rozmiar wektora mógł być dynamiczny
for (size_t i=0; i<v.size(); ++i){
    v.at(i) = i;
    cout << "Dodano: " << i << endl;
}

// 3. Wypisz ilość elementów w wektorze
cout << "Ilość elementów w wektorze: " << v.size() << "\n";

// 4. Usuń elementy od 3 do 7 i wyświetl wynik.
v.erase(v.begin()+3, v.begin()+8);
for (size_t i=0; i<v.size(); ++i){
cout << v[i] << endl;
}

// 5. Usuń ostatni element wektora i wyświetl wynik.
v.pop_back();
cout << "\nPo usunięciu ostatniego elementu: \n";
for (size_t i=0; i<v.size(); ++i){
cout << v[i] << endl;
}

// 6. Wstaw na początek wektora liczbę 102 i wyświetl wynik
v.insert(v.begin(), 102);
cout << "\nPo dodaniu pierwszego elementu: \n";
for (size_t i=0; i<v.size(); ++i){
cout << v[i] << endl;
}

// 7. Wstaw na koniec wektora liczbę 110011 i wyświetl wynik
v.push_back(stoi("110011"));
cout << "\nPo dodaniu liczby na końcu: \n";
for (size_t i=0; i<v.size(); ++i){
cout << v[i] << endl;
}
}