#include <iostream>
#include <queue>

std::queue<int> reduce_queue(std::queue<int> queue, int max_len)
{
    if (queue.size() > max_len)
    {
        std::cout << "Kolejka jest pełna. Usuwanie nadmiarowych elementów \n";
        const int items_to_remove = queue.size() - max_len;
        for (int i = 0; i <= items_to_remove; i++)
        {
            queue.pop();
        }
        return queue;
    }
    else
    {
        return queue;
    }
}
void print_queue(std::queue<int> queue)
{
    std::cout << "Zawartość kolejki: \n";
    while (!queue.empty())
    {
        std::cout << " " << queue.front();
        queue.pop();
    }
    std::cout << std::endl;
}

int main(int argc, char const *argv[])
{
    // przykład kolejki LIFO
    std::queue<int> kolejka;

    // 1.
    // Napisz program wstawiający do kolejki liczby
    // naturalne od 1 do 12. Wyświetl zawartość kolejki na ekran.
    for (size_t i = 1; i < 13; i++)
    {
        kolejka.push(i);
        std::cout << "Dodano: " << i << std::endl;
    }

    // 2.
    // Podaj długość kolejki używając metody size
    std::cout << kolejka.size() << std::endl;

    // 3.
    // Usuń z kolejki 5 elementów.
    // Wyświetl zawartość kolejki na ekran i podaj jej długość
    for (size_t i = 0; i < 4; i++)
    {
        kolejka.pop();
    }
    print_queue(kolejka);
    std::cout << kolejka.size() << std::endl;

    // 4.
    // Uzupełnić program o funkcje ograniczającą długość kolejki do zadanej wartości np.
    // 6 i usuwającej elementy przekraczające ograniczenie.
    // W przypadku gdy przekroczone jest ograniczenie funkcja ma wyświetlać informację,
    // ze kolejka jest pełna a element jest usuwany

    kolejka = reduce_queue(kolejka, 6);
    print_queue(kolejka);

    // 5.
    // Wykorzystując opracowaną funkcję dodać do kolejki 4 elementy.
    kolejka.push(12);
    kolejka.push(232);
    kolejka.push(54);
    kolejka.push(56);

    // 6.
    // Wyświetl zawartość kolejki na ekran i podaj jej długość.
    print_queue(kolejka);
    std::cout << kolejka.size() << std::endl;

    // 7.
    // Usunąć z kolejki 2 elementy.
    kolejka.pop();
    kolejka.pop();

    // 8.
    // Wyświetl zawartość kolejki na ekran i podaj jej długość.
    print_queue(kolejka);
    std::cout << kolejka.size() << std::endl;

    // 9.
    // Dodać do kolejki 7 elementów

    for (int i = 0; i < 7; i++)
    {
        kolejka.push(i);
    }

    // 9.
    // Wyświetl zawartość kolejki na ekran i podaj jej długość.
    print_queue(kolejka);
    std::cout << kolejka.size() << std::endl;

    return 0;
}