

# 1
```
ćw. 1/z1.cpp: In function ‘int main()’:
ćw. 1/z1.cpp:32:16: error: ‘void student::printOpis()’ is private within this context
 stud.printOpis();
                ^
ćw. 1/z1.cpp:24:6: note: declared private here
 void student::printOpis()
      ^~~~~~~
```

```diff
diff --git "a/\304\207w. 1/z1.cpp" "b/\304\207w. 1/z1.cpp"
index 36db323..ab768f2 100644
--- "a/\304\207w. 1/z1.cpp"     
+++ "b/\304\207w. 1/z1.cpp"     
@@ -4,8 +4,8 @@ using namespace std;
 class student
 {
 private:
-void printOpis();
 public:
+void printOpis();
 student();
 string Opis_ = "student grupy";
 };
 ```

### 2. Dodanie dziedziczenia

```diff
diff --git "a/\304\207w. 1/z1.cpp" "b/\304\207w. 1/z1.cpp"
index ab768f2..c936fe4 100644
--- "a/\304\207w. 1/z1.cpp"     
+++ "b/\304\207w. 1/z1.cpp"     
@@ -10,7 +10,7 @@ student();
 string Opis_ = "student grupy";
 };
 
-class starosta
+class starosta : public student
 {
 public:
 string Opis_ = "starosta grupy";
```

### 3. 4. Korzystanie z dziedziczonej metody 

```diff
diff --git "a/\304\207w. 1/z1.cpp" "b/\304\207w. 1/z1.cpp"
index ab768f2..d4b7fc1 100644
--- "a/\304\207w. 1/z1.cpp"     
+++ "b/\304\207w. 1/z1.cpp"     
@@ -10,7 +10,7 @@ student();
 string Opis_ = "student grupy";
 };
 
-class starosta
+class starosta : public student
 {
 public:
 string Opis_ = "starosta grupy";
@@ -28,6 +28,6 @@ cout << "Opis: " << Opis_ << endl;
 
 int main()
 {
-student stud;
-stud.printOpis();
+starosta star;
+star.printOpis();
 }
```
Wynik
```
[pwozny@pcte25572 cpp]$ g++ -g ćw.\ 1/z1.cpp -o out
[pwozny@pcte25572 cpp]$ ./out 
Tworzenie obiektu klasy student o nazwie: student grupy
Opis: student grupy
```


## z2.cpp

### 1. Kolejność wykonywania konstruktorów

```diff
diff --git "a/\304\207w. 1/z1.cpp" "b/\304\207w. 1/z1.cpp"
index d4b7fc1..226e497 100644
--- "a/\304\207w. 1/z1.cpp"     
+++ "b/\304\207w. 1/z1.cpp"     
@@ -14,6 +14,9 @@ class starosta : public student
 {
 public:
 string Opis_ = "starosta grupy";
+void printOpis(void ){
+cout << "Tworzenie obiektu klasy startosta o nazwie: " << Opis_ << endl;
+}
 };
```
Wynik
```
[pwozny@pcte25572 cpp]$ ./out 
Tworzenie obiektu klasy student o nazwie: student grupy
Tworzenie obiektu klasy startosta o nazwie: starosta grupy
```






# 2

```diff

diff --git "a/\304\207w. 1/z3.cpp" "b/\304\207w. 1/z3.cpp"
index 8d2b767..f86d964 100644
--- "a/\304\207w. 1/z3.cpp"     
+++ "b/\304\207w. 1/z3.cpp"     
@@ -4,8 +4,8 @@ using namespace std;
 class student
 {
     
-public:
 string imie_nazwisko_ = "NO_NAME";
+public:
 
 unsigned int nr_indeksu_ = 0;
 
```

# 3 

```
pwozny@khorinis:~/cernbox/workspace/agh/cpp/ćw. 1$ g++ z3.cpp 
z3.cpp: In function ‘int main()’:
z3.cpp:54:25: error: ‘std::string student::imie_nazwisko_’ is private within this context
   54 | cout << "Dane:" << stud.imie_nazwisko_ << " " << stud.nr_indeksu_ << endl;
      |                         ^~~~~~~~~~~~~~
z3.cpp:7:8: note: declared private here
    7 | string imie_nazwisko_ = "NO_NAME";
      |        ^~~~~~~~~~~~~~
z3.cpp:56:25: error: ‘std::string student::imie_nazwisko_’ is private within this context
   56 | cout << "Dane:" << star.imie_nazwisko_ << " " << star.nr_indeksu_ << endl;
      |                         ^~~~~~~~~~~~~~
z3.cpp:7:8: note: declared private here
    7 | string imie_nazwisko_ = "NO_NAME";
      |        ^~~~~~~~~~~~~~
```

# 4
```diff
diff --git "a/\304\207w. 1/z3.cpp" "b/\304\207w. 1/z3.cpp"
index f86d964..c035c76 100644
--- "a/\304\207w. 1/z3.cpp"     
+++ "b/\304\207w. 1/z3.cpp"     
@@ -51,7 +51,7 @@ int main()
 student stud("Jan Kowalski",7);
 stud.printOpis();
 
-cout << "Dane:" << stud.imie_nazwisko_ << " " << stud.nr_indeksu_ << endl; 
+stud.printDane(); 
 starosta star("Aleksandra Nowak",999,"mail@nomail.dot"); star.printOpis();
-cout << "Dane:" << star.imie_nazwisko_ << " " << star.nr_indeksu_ << endl;
+star.printDane();
```

```
pwozny@khorinis:~/cernbox/workspace/agh/cpp/ćw. 1$ ./a.out 
Tworzenie obiektu klasy student o nazwie: student grupy
Opis: student grupy
 Metoda printDane klasy bazowej
 imie nazwisko Jan Kowalski
 nr indeksu 7
Tworzenie obiektu klasy student o nazwie: student grupy
Tworzenie obiektu klasy starosta o nazwie: starosta grupy
Opis: student grupy
 Metoda printDane klasy bazowej
 imie nazwisko Aleksandra Nowak
 nr indeksu 999
 ```

 # 6
```diff
diff --git "a/\304\207w. 1/z3.cpp" "b/\304\207w. 1/z3.cpp"
index c035c76..e6b9d95 100644
--- "a/\304\207w. 1/z3.cpp"     
+++ "b/\304\207w. 1/z3.cpp"     
@@ -4,8 +4,8 @@ using namespace std;
 class student
 {
     
-string imie_nazwisko_ = "NO_NAME";
 public:
+string imie_nazwisko_ = "NO_NAME";
 
 unsigned int nr_indeksu_ = 0;
 
@@ -24,7 +24,15 @@ class starosta : public student
 public:
 string email_ = "no@noemail";
 
+void printDane()
+{
+cout << " Metoda printDane klasy dziedziczonej" << endl;
+cout << " imie nazwisko " << student::imie_nazwisko_ << endl;
+cout << " nr indeksu " << student::nr_indeksu_ << endl;
+cout << " adres email " << email_ << endl;
+}
 };
 
 starosta::starosta(string imie_nazwisko, unsigned int nr_indeksu, string email) :
 ```

 ```
 Tworzenie obiektu klasy student o nazwie: student grupy
Opis: student grupy
 Metoda printDane klasy bazowej
 imie nazwisko Jan Kowalski
 nr indeksu 7
Tworzenie obiektu klasy student o nazwie: student grupy
Tworzenie obiektu klasy starosta o nazwie: starosta grupy
Opis: student grupy
 Metoda printDane klasy dziedziczonej
 imie nazwisko Aleksandra Nowak
 nr indeksu 999
```

